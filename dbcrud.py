import cx_Oracle


dsn = cx_Oracle.makedsn(host='59.2.0.80',sid='pidev', port=1522)
conn = cx_Oracle.connect(user='dbtest', password='dbtest', dsn=dsn)
cur = conn.cursor()

def viewuser(id):
    cur.execute("select users.id,users.username,profiles.lastname,profiles.firstname from users left join profiles on users.id=profiles.user_id where users.id =" + str(id) )
    for row in cur :
        display ="USER ID : %s USERNAME : %s LASTNAME : %s FIRSTNAME : %s " % (str(row[0]),row[1],row[2],row[3])
        return display
    return "ID does not exist"

def getlastid():
    cur.execute("select id from  (select * from users order by id desc) where ROWNUM <= 1")
    for row in cur :
        return row[0]

def insertuser(username,password):
    cur.execute("insert into users (username,password)values ('" + username + "','" + password + "')")
    conn.commit()

def insertprofiles(lastname,firstname):
    userid = getlastid()
    cur.execute("insert into profiles (user_id,firstname,lastname)values (" + str(userid) + ",'" + firstname + "','" + lastname + "')")
    conn.commit()

def updateuser(update_userid,u_username,u_password,u_firstname,u_lastname):
    cur.execute("update users set username='" + u_username + "', password='" + u_password + "' where id='" + str(update_userid) + "'")
    cur.execute("update profiles set firstname='" + u_firstname + "', lastname='" + u_lastname + "' where user_id='" + str(update_userid) + "'")
    cur.execute("commit")

def deleteuser(delete_id):
    cur.execute("delete from users where id='" + str(delete_id) + "'")
    cur.execute("delete from profiles where user_id='" + str(delete_id) + "'")
    cur.execute("commit")

def insertbooks(author,book_name,pub_year):
    cur.execute("insert into books (author,book_name,published_year) values ('" + author + "','" + book_name + "','" + str(pub_year) + "')")
    cur.execute("commit")
