from __future__ import division
# -*- coding: utf-8 -*-

def celtofah(num):
    try:
        cel = float(num)
        resfah = cel * (9/5) + 32
        return '%.4f' % resfah
    except ValueError:
        return "Could not convert "+str(num)

def celtokel(num):
    try:
        cel = float(num)
        reskel = cel + 273.15
        return '%.4f' % reskel
    except ValueError:
        return "Could not convert "+str(num)

def fahtocel(num):
    try:
        fah = float(num)
        rescel = (fah-32) * (5/9)
        return '%.4f' % rescel
    except ValueError:
        return "Could not convert "+str(num)

def fahtokel(num):
    try:
        fah = float(num)
        reskel = (fah + 459.67) * (5/9)
        return '%.4f' % reskel
    except ValueError:
        return "Could not convert "+str(num)

def keltocel(num):
    try:
        kel = float(num)
        rescel = kel - 273.15
        return '%.4f' % rescel
    except ValueError:
        return "Could not convert "+str(num)

def keltofah(num):
    try:
        kel = float(num)
        resfah = (kel * (9/5)) - 459.67
        return '%.4f' % resfah
    except ValueError:
        return "Could not convert "+str(num)