import cx_Oracle
import uuid
import hashlib
import sqlite3

"""
def connect():
    dsn = cx_Oracle.makedsn(host='59.2.0.80', sid='pidev', port=1522)
    conn = cx_Oracle.connect(user='dbtest', password='dbtest', dsn=dsn)
    cur = conn.cursor()
    return cur, conn

cur, conn = connect()
"""
def connect():
    conn = sqlite3.connect('C:\Users\malcorin\Documents\PY\Tkinter\\tinkerdatabase')
    return conn

def connect2():
    conn2 = sqlite3.connect('C:\Users\malcorin\Documents\PY\\readurl\\forex')
    return conn2

conn2 = connect2()
conn = connect()

def executesql(sql):
    cursor = conn.execute(sql)
    conn.commit()
    return cursor

def executesql2(sql):
    cursor2 = conn2.execute(sql)
    conn2.commit()
    return cursor2

def hash_password(password):
    # uuid is used to generate a random number
    salt = uuid.uuid4().hex
    return hashlib.md5(salt.encode() + password.encode()).hexdigest() + ':' + salt

def check_password(hashed_password, user_password):
    password, salt = hashed_password.split(':')
    return password == hashlib.md5(salt.encode() + user_password.encode()).hexdigest()
